import process from "process";
import fs from "fs";

main(...process.argv.slice(2)).catch((err) => {
  console.error(err);
  process.exit(1);
});

async function main(fromExt, toExt, ...files) {
  await Promise.all(files.map(replaceImportExtensions(fromExt, toExt)));
}

function replaceImportExtensions(fromExt, toExt) {
  return async (file) => {
    let mod = await fs.promises.readFile(file, { encoding: "utf8" });
    let lines = mod.split("\n").map((line) => {
      if (!line.startsWith("import")) return line;
      if (fromExt) return line.replace(fromExt, toExt);
      return line.replace(
        /(['"])(;)?$/,
        (_, qm, semi) => `${toExt}${qm}${semi}`,
      );
    });
    await fs.promises.writeFile(file, lines.join("\n"));
  };
}
