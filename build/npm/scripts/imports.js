import fs from "fs";

const DEPS_FILE = "./src/deps.ts";
const DEPS_MAP = "./deps.json";
const PACKAGE_JSON = "./package.json";

const exportRegex = /export +.+ +from +['"]([^'"]+)['"];?/g;
const importRegex = /import +['"]([^'"]+)['"];?/g;
const readFile = (path) => fs.promises.readFile(path, { encoding: "utf8" });

main().catch((err) => console.error(err));

async function main() {
  if (!fs.existsSync(DEPS_MAP)) return;
  const depsMap = JSON.parse(await readFile(DEPS_MAP));
  await addDepsToPkgJson(depsMap);
  let deps = await readFile(DEPS_FILE);
  const replacer = (match, url) =>
    match.replace(url, depsMap[url].split("@")[0]);
  deps = deps.replace(exportRegex, replacer);
  deps = deps.replace(importRegex, replacer);
  await fs.promises.writeFile(DEPS_FILE, deps);
}

async function addDepsToPkgJson(depsMap) {
  const pkgJson = JSON.parse(await readFile(PACKAGE_JSON));
  pkgJson.dependencies = Object.keys(depsMap).reduce((acc, key) => {
    const [name, version] = depsMap[key].split("@");
    acc[name] = version;
    return acc;
  }, {});
  await fs.promises.writeFile(PACKAGE_JSON, JSON.stringify(pkgJson, null, 2));
}
