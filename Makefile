runtest:
	- docker build -f ./build/Dockerfile.test -t loudmaptest:latest .
	- docker create --name loudmaptest loudmaptest:latest
	- docker run loudmaptest lint --unstable
	- docker run loudmaptest
	- docker rm loudmaptest
transpile:
	- docker build -f ./build/Dockerfile.npm -t loudmapnpm:latest .
	- docker create --name loudmapnpm loudmapnpm:latest
	- docker cp loudmapnpm:/app/lib ./lib
	- docker rm loudmapnpm
clean:
	- docker image rm loudmaptest:latest loudmapnpm:latest