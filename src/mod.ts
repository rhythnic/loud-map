import { EventEmitter } from "./deps.ts";

interface ComputeHandler {
  (...args: unknown[]): unknown;
}

interface ComputeContainer {
  deps: string[];
  fn: ComputeHandler;
}

interface Batch {
  [propName: string]: unknown;
}

export class LoudMap extends Map<string, unknown> {
  _computed: Map<string, ComputeContainer>;
  _inverseComputedDependencies: Map<string, string[]>;
  _eventSource: EventEmitter;
  constructor() {
    super();
    this._eventSource = new EventEmitter();
    this._computed = new Map();
    this._inverseComputedDependencies = new Map();
  }
  static from(map: Map<string, unknown>): LoudMap {
    let result = new LoudMap();
    map.forEach((val, key) => {
      result.set(key, val, false);
    });
    return result;
  }
  get events(): EventEmitter {
    return this._eventSource;
  }
  get(key: string): unknown {
    let val = super.get(key);
    if (typeof val === "undefined" && this._computed.has(key)) {
      return cacheComputedValue(this, key);
    }
    return val;
  }
  set(key: string, value: unknown, emitGlobalChange = true): this {
    const oldValue = super.get(key);
    if (typeof value !== "undefined") super.set(key, value);
    else super.delete(key);
    let inverseDeps = this._inverseComputedDependencies.get(key);
    if (inverseDeps) inverseDeps.forEach((x) => this.delete(x, false));
    this.events.emit(`change/${key}`, value, oldValue);
    if (emitGlobalChange) {
      this.events.emit("change", { [key]: value });
    }
    return this;
  }
  delete(key: string, emitGlobalChange = true): boolean {
    const existed = this.has(key);
    this.set(key, void 0, emitGlobalChange);
    return existed;
  }
  batch(changes: Batch): this {
    const keys = Object.keys(changes);
    keys.forEach((key) => {
      this.set(key, changes[key], false);
    });
    this.events.emit("change", changes);
    return this;
  }
  compute(key: string, deps: string[], fn: ComputeHandler): this {
    this._computed.set(key, { deps, fn });
    deps.forEach((dep) => {
      const computedKeys = this._inverseComputedDependencies.get(dep) || [];
      this._inverseComputedDependencies.set(dep, [...computedKeys, key]);
    });
    return this;
  }
}

export function cacheComputedValue(lMap: LoudMap, key: string): unknown {
  const computed = lMap._computed.get(key);
  if (!computed) {
    throw new Error(
      `Attempted to compute cache for unknown computed key ${key}`,
    );
  }
  let val = computed.fn(...computed.deps.map((x: string) => lMap.get(x)));
  lMap.set(key, val, false);
  return val;
}
