import { LoudMap } from "../src/mod.ts";
import { assert, assertEquals, assertStrictEquals } from "./deps.ts";

function stub() {
  const _stub = (...args) => {
    _stub.calls.push(args);
  };
  _stub.calls = [];
  return _stub;
}

Deno.test("creates a LoudMap from a regular Map", () => {
  const map = new Map();
  map.set("a", 0);
  const loudMap = LoudMap.from(map);
  assert(loudMap instanceof LoudMap);
  assertStrictEquals(loudMap.get("a"), 0);
});

Deno.test("returns a stored value", () => {
  const map = new LoudMap();
  map.set("a", 0);
  assertStrictEquals(map.get("a"), 0);
});

Deno.test("computes a computed value if not in cache", () => {
  const map = new LoudMap();
  map.compute("b", ["a"], (a) => a + 1);
  map.set("a", 0);
  assertStrictEquals(map.get("b"), 1);
});

Deno.test("sets a value in the Map", () => {
  const map = new LoudMap();
  map.set("a", 0);
  assertStrictEquals(map.get("a"), 0);
});

Deno.test("deletes the key from map if value is undefined", () => {
  const map = new LoudMap();
  map.set("a", 0);
  map.set("a", void 0);
  const iterator = map.keys();
  assertStrictEquals(typeof iterator.next().value, "undefined");
});

Deno.test("emits a key-specific change event", () => {
  return new Promise((resolve, reject) => {
    try {
      const map = new LoudMap();
      map.set("a", 0);
      map.events.on("change/a", (val, oldVal) => {
        assertStrictEquals(val, 1);
        assertStrictEquals(oldVal, 0);
        resolve();
      });
      map.set("a", 1);
    } catch (error) {
      reject(error);
    }
  });
});

Deno.test("emits a a global change event if isolated arg is true", () => {
  return new Promise((resolve, reject) => {
    try {
      const map = new LoudMap();
      map.events.on("change", (changes) => {
        assertStrictEquals(changes.a, 0);
        resolve();
      });
      map.set("a", 0, true);
    } catch (error) {
      reject(err);
    }
  });
});

Deno.test("calls set method with undefined value", () => {
  const map = new LoudMap();
  map.set = stub();
  map.delete("a");
  assertEquals(map.set.calls[0], ["a", void 0, true]);
});

Deno.test("sets multiple values at the same time", () => {
  const map = new LoudMap();
  map.batch({
    a: 0,
    b: 1,
  });
  assertStrictEquals(map.get("a"), 0);
  assertStrictEquals(map.get("b"), 1);
});

Deno.test("adds a computed property", async () => {
  const map = new LoudMap();
  map.compute("b", ["a"], (a) => a + 1);
  map.compute("c", ["b"], (b) => Promise.resolve(b + 1));
  map.set("a", 5);
  assertStrictEquals(map.get("b"), 6);
  assertStrictEquals(await map.get("c"), 7);
});
